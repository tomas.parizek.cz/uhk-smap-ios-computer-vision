//
//  CGRect.swift
//  UHK AI
//
//  Created by Tomáš Pařízek on 22.10.2023.
//

import UIKit

extension CGRect {
    func rotated(by image: UIImage) -> CGRect {
        switch image.imageOrientation {
        case .left:
            return CGRect(
                x: origin.y,
                y: image.size.height - origin.x - width,
                width: height,
                height: width
            )
        case .right:
            return CGRect(
                x: image.size.width - origin.y - height,
                y: origin.x, width: height,
                height: width
            )
        case .down:
            var result = self
            result.origin.x = image.size.width - maxX
            result.origin.y = image.size.height - maxY
            return result
        default:
            return self
        }
    }
}
