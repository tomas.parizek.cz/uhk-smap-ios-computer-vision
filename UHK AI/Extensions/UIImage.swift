//
//  UIImage.swift
//  UHK AI
//
//  Created by Tomáš Pařízek on 22.10.2023.
//

import UIKit

extension UIImage {
    func withBoundingBoxes(_ boxes: [CGRect]) -> UIImage {
        UIGraphicsImageRenderer(size: size).image { renderer in
            self.draw(at: .zero)

            let ctx = renderer.cgContext

            ctx.setStrokeColor(UIColor.yellow.cgColor)
            ctx.setLineWidth(10)

            for box in boxes {
                ctx.stroke(box)
            }
        }
    }
}
