//
//  UIViewContoller.swift
//  UHK AI
//
//  Created by Tomáš Pařízek on 22.10.2023.
//

import UIKit

extension UIViewController {
    func handleError(_ error: Error) {
        let alert = UIAlertController(
            title: "Error",
            message: error.localizedDescription,
            preferredStyle: .alert
        )
        let okAction = UIAlertAction(
            title: "Ok",
            style: .cancel
        )
        alert.addAction(okAction)
        present(alert, animated: true)
    }
}
