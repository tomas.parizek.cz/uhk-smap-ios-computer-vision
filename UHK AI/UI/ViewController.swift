//
//  ViewController.swift
//  UHK AI
//
//  Created by Tomáš Pařízek on 18.09.2023.
//

import Combine
import Foundation
import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var numberOfFaces: UILabel!

    private let visionDetector = VisionDetector()
    private let coreImageDetector = CoreImageDetector()
    private let blazeDetector = BlazeDetector()

    private let imageSubject = CurrentValueSubject<UIImage?, Never>(nil)
    private let facesSubject = CurrentValueSubject<[FaceDetection], Never>([])
    private let detectionTimeSubject = CurrentValueSubject<CFAbsoluteTime?, Never>(nil)
    private var pathLayer: CALayer?
    private var cancellables = Set<AnyCancellable>()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBinding()
    }

    @IBAction func pickImageButtonTapped(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.delegate = self
        present(picker, animated: true)
    }
    
    @IBAction func detectButtonTapped(_ sender: Any) {
        detectFaces(using: visionDetector)
    }
    
    @IBAction func ciDetectButtonTapped(_ sender: Any) {
        detectFaces(using: coreImageDetector)
    }
    @IBAction func blazeDetectButtonTapped(_ sender: Any) {
        detectFaces(using: blazeDetector)
    }
}

private extension ViewController {
    func setupBinding() {
        imageSubject
            .sink(receiveValue: { [weak self] _ in
                self?.facesSubject.send([])
                self?.detectionTimeSubject.send(nil)
            })
            .store(in: &cancellables)

        Publishers.CombineLatest(facesSubject, detectionTimeSubject)
            .map {
                guard let detectionTime = $1
                else { return "-" }
                
                return "\($0.count) face(s) detected in \((detectionTime*1000).rounded()/1000)s"
            }
            .assign(to: \.text, on: numberOfFaces)
            .store(in: &cancellables)

        Publishers.CombineLatest(imageSubject, facesSubject)
            .sink(receiveValue: { [weak self] image, faces in
                let boxes = faces.map { $0.boundingBox }
                self?.imageView.image = image?.withBoundingBoxes(boxes)
            })
            .store(in: &cancellables)
    }

    func detectFaces(using detector: FaceDetector) {
        guard let image = imageSubject.value
        else { return }

        Task {
            do {
                let detectionStartTime = CFAbsoluteTimeGetCurrent()
                let detectionResult = try await detector.detectFaces(in: image)
                let executionTime = CFAbsoluteTimeGetCurrent() - detectionStartTime
                detectionTimeSubject.send(executionTime)
                facesSubject.send(detectionResult)
            } catch {
                handleError(error)
            }
        }
    }
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage
        else { return }
        
        imageSubject.send(image)

        dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}

