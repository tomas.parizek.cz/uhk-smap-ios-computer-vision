//
//  FaceDetector.swift
//  UHK AI
//
//  Created by Tomáš Pařízek on 22.10.2023.
//

import UIKit

protocol FaceDetector {
    func detectFaces(in image: UIImage) async throws -> [FaceDetection]
}
