//
//  BlazeDetector.swift
//  UHK AI
//
//  Created by Tomáš Pařízek on 22.10.2023.
//

import MediaPipeTasksVision
import UIKit

class BlazeDetector: FaceDetector {
    private enum Constants {
        static let modelAssetPath = Bundle.main.path(forResource: "blaze_face_short_range", ofType: "tflite")!
    }

    func detectFaces(in image: UIImage) async throws -> [FaceDetection] {
        let faceDetectorOptions = FaceDetectorOptions()
        faceDetectorOptions.runningMode = .image
        faceDetectorOptions.baseOptions.modelAssetPath = Constants.modelAssetPath

        let faceDetector = try MediaPipeTasksVision.FaceDetector(options: faceDetectorOptions)
        
        let mpImage = try MPImage(uiImage: image)
        let result = try faceDetector.detect(image: mpImage)

        let faceDetections = result.detections.map { detection in
            FaceDetection(
                boundingBox: detection.boundingBox.rotated(by: image),
                confidence: nil
            )
        }

        return faceDetections
    }
}
