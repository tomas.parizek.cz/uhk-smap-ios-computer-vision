//
//  CoreImageDetector.swift
//  UHK AI
//
//  Created by Tomáš Pařízek on 22.10.2023.
//

import UIKit

class CoreImageDetector: FaceDetector {
    func detectFaces(in image: UIImage) async throws -> [FaceDetection] {
        let ciImage = CIImage(cgImage: image.cgImage!)

        let options = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: options)!

        let faces = faceDetector.features(in: ciImage)

        let faceDetections = faces.map {
            FaceDetection(
                boundingBox: CGRect(
                    x: $0.bounds.origin.x,
                    y: image.size.height - $0.bounds.origin.y - $0.bounds.size.height,
                    width: $0.bounds.size.width,
                    height: $0.bounds.size.height
                ),
                confidence: nil
            )
        }

        return faceDetections
    }
}
