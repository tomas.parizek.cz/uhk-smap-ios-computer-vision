//
//  VisionDetector.swift
//  UHK AI
//
//  Created by Tomáš Pařízek on 22.10.2023.
//

import Combine
import UIKit
import Vision

class VisionDetector: FaceDetector {
    private let results: CurrentValueSubject<Result<[FaceDetection], Error>?, Never> = .init(nil)
    private var cancellables: Set<AnyCancellable> = []

    func detectFaces(in image: UIImage) async throws -> [FaceDetection] {
        results.send(nil)

        let request = VNDetectFaceRectanglesRequest {
            self.handleDetectedFaces(
                image: image,
                request: $0,
                error: $1
            )
        }
        let imageRequestHandler = VNImageRequestHandler(cgImage: image.cgImage!)
        try imageRequestHandler.perform([request])

        return try await withCheckedThrowingContinuation { continuetion in
            results
                .compactMap { $0 }
                .first()
                .sink(receiveValue: { result in
                    switch result {
                    case .success(let faceDetections):
                        continuetion.resume(returning: faceDetections)
                    case .failure(let error):
                        continuetion.resume(throwing: error)
                    }
                })
                .store(in: &cancellables)
        }
    }

    private func handleDetectedFaces(image: UIImage, request: VNRequest?, error: Error?) {
        if let error {
            results.send(.failure(error))
            return
        }

        guard let faces = request?.results as? [VNFaceObservation]
        else {
            results.send(.success([]))
            return
        }

        let faceDetections = faces.map { detection in
            let detectionBounds = CGRect(
                x: detection.boundingBox.minX * image.size.width,
                y: (1 - detection.boundingBox.maxY) * image.size.height,
                width: detection.boundingBox.width * image.size.width,
                height: detection.boundingBox.height * image.size.height
            )
            return FaceDetection(
                boundingBox: detectionBounds,
                confidence: detection.confidence
            )
        }

        results.send(.success(faceDetections))
    }
}
