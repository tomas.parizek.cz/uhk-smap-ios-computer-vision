//
//  FaceDetection.swift
//  UHK AI
//
//  Created by Tomáš Pařízek on 22.10.2023.
//

import UIKit

struct FaceDetection {
    let boundingBox: CGRect
    let confidence: Float?
}
